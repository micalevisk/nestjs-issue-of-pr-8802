import { NestFactory } from '@nestjs/core';
import { FastifyAdapter } from '@nestjs/platform-fastify';

import { AppModule } from './app.module';

async function bootstrap() {
  const appExpress = await NestFactory.create(AppModule);
  const appFastify = await NestFactory.create(AppModule, new FastifyAdapter());

  await appExpress.listen(3000, () => { console.log(`express listening on 3000`) });
  await appFastify.listen(4000, () => { console.log(`fastify listening on 4000`) });
}

bootstrap();
