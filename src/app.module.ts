import { Module, Res, Controller, Get } from '@nestjs/common';

@Controller()
export class AppController {
  @Get()
  hello(@Res({ passthrough: !true }) res: any) {
    const isFastify = !!res.raw;

    if (isFastify) {
      res.raw.end('any string')
      // res.raw.end(undefined)
      // res.raw.end(null)
    } else {
      res.end('any string')
      // res.end(undefined)
      // res.end(null)
    }

    throw new Error('triggers the native exception filter');
  }
}

@Module({
  controllers: [AppController],
})
export class AppModule {}
