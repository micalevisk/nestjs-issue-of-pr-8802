import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { FastifyAdapter, NestFastifyApplication } from '@nestjs/platform-fastify';
import { ExpressAdapter } from '@nestjs/platform-express';
import { AppModule } from '../src/app.module';

describe('AppController (e2e)', () => {
  describe('Express', () => {
    let appExpress: INestApplication;

    beforeAll(async () => {
      const moduleFixture = await Test.createTestingModule({
        imports: [AppModule],
      }).compile();

      appExpress = moduleFixture.createNestApplication(new ExpressAdapter(), {
        logger: false,
      });

      await appExpress.init();
    });

    afterAll(() => {
      return  appExpress.close();
    })

    test('GET / should reply with 200 OK', () => {
      return request(appExpress.getHttpServer())
        .get('/')
        .expect(200)
        .expect('ok');
    });
  });


  describe('Fastify', () => {
    let appFastify: NestFastifyApplication;

    beforeAll(async () => {
      const moduleFixture = await Test.createTestingModule({
        imports: [AppModule],
      }).compile();

      appFastify = moduleFixture.createNestApplication(new FastifyAdapter(), {
        logger: false,
      });

      await appFastify.init();
    });

    afterAll(() => {
      return appFastify.close();
    })

    test('GET / should reply with 200 OK', () => {
      return request(appFastify.getHttpServer())
        .get('/')
        .expect(200)
        .expect('ok');
    });
  });
});
